source("../R/functions.R")
png("showProbalJ.png")
layout(matrix(1:2,nrow=1,ncol=2),c(.8,.2))
Js=seq(0,2,.05)
p_i=seq(0,1,.005)
exp=list(A=c(1,2,5,20),B=seq(1,50,length.out=5),C=c(3,4,3,6,4))
for(ne in names(exp)){
    for(psi in c(0,1)){
        png(paste0("showProba",ne,"_psi",psi,".png"))
        Js=seq(0,2,.05)
        p0=exp[[ne]]
        p0=p0/sum(p0)
        simu=sapply(Js,function(j)apply(replicate(500,p(p_t=p0,J=j,u=rep(0,length(p0)),beta=0,sde=psi)),1,mean))
        title=paste("proba for different J with given set of freq\n(beta=0,psi=",psi,")")
        plotProbas(p0,Js,simu,title)
        dev.off()
    }
}

p0=runif(100)
p0=p0/sum(p0)
replicate(1000,sum(round(p(p_t=p0,J=2,u=rep(0,length(p0)),beta=1,sde=1)*5000)))



plotProbas <- function(p0,Js,simu,title=title){
    oldpar=par()
    layout(matrix(1:2,nrow=1,ncol=2),c(.8,.2))
    m=length(p0)
    #cls=rainbow(m)
    cls=2:(m+1)
    plot(1,1,type="n",xlim=range(Js),ylim=c(0,1),ylab="Pr(i)",xlab="J",main=title)
    abline(v=1)
    for(i in 1:m){
        points(Js,simu[i,],col=cls[i],pch=20)
    }
    text(rep(1,m),p0+0.01,paste0("t",1:m,":",round(p0*100),"%"))
    points(rep(1,m),p0,col=cls,pch=4,cex=1.1)
    legend("toplef",pch=c(20,4),legend=c("probability to select a traits","real frequency of the trait"),cex=.8)

    par(mar=c(5,0,5,1))
    barplot(as.matrix(p0),beside=F,col=cls,axes=F,main="")
    mtext("p_i",3,0)

    text(rep(.5,m),cumsum(p0)-.5*p0,paste0("t",1:m,":",round(p0*100),"%"))
    par(oldpar)
}
