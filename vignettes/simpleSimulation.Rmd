---
title: "simpleSimulation"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{simpleSimulation}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(popVSutil)

```

The easiest way to output a simulation:
          
```{r}
J=.1
beta=.1
simpres=model(p0=rep(round(50/4),4),J=J,u0="runif",beta=beta,tstep=200,mu=0.1,N=500,m=4,sde=1)
utilityHM(simpres,main=titleJB(J=J,beta=beta))
plotCCFD(simpres$freq,ylim=c(0.0001,1))
```
Let's draw a few of them:
```{r,width.out="50%"}
Js=seq(0,1.2,length.out = 2)
betas=seq(0,1.2,length.out = 2)
for(J in Js){
    for(beta in betas){
        simpres=model(p0=rep(round(50/4),4),J=J,u0="runif",beta=beta,tstep=200,mu=0.1,N=500,m=4,sde=1)
        utilityHM(simpres,main=titleJB(J=J,beta=beta))
    }
}
```
