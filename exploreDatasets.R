camera=read.csv("Spanish_slrs_sales_units.csv")
camera=camera[,-1]
camera=apply(camera,2,unlist) 
camera[is.na(camera)]=0
tots=apply(camera,2,sum)
camera.prop=apply(camera,1,function(i)i/tots) 
plot(tots,type="l")
abline(v=grep("W01",colnames(camera)),col="red")
plot(camera.prop[,4],type="l")

plot(1,1,type="n",ylim=range(camera.prop,na.rm=T),xlim=c(0,150))
apply(camera.prop,2,lines)


load("data/ceramics_lbk_merzbach.rda")
load("data/armature_types_france.rda")
rownames(ceramics_lbk_merzbach)=ceramics_lbk_merzbach[,1]
ceramics_lbk_merzbach=t(ceramics_lbk_merzbach[,-1])
rownames(armature_types_france)=armature_types_france[,1]
armature_types_france=t(armature_types_france[,-1])
saveRDS(file="data/ceramics_lbk_merzbach.RDS",ceramics_lbk_merzbach)
saveRDS(file="data/armature_types_france.RDS",armature_types_france)
saveRDS(file="data/camera_spain.RDS",camera)


