---
title: "4 box model new xplorationts - EXPLORE ABC results"
author: "Simon"
date: "`r Sys.Date()`"    
output:
  html_document:
    code_folding: show    # hide / show, default option for the code display
    theme: default        # the Bootstrap theme to use for the page
    number_sections: yes  # whether add number index before section header
---

```{r,echo=F}
source("R/../../R/functions.R")
knitr::opts_chunk$set(warning=FALSE,message=FALSE,fig.align="center",cache=TRUE,collapse=TRUE,fig.show="hold",results="hide")

originalparams=list(
	A=list(beta=0,J=0.1),
	B=list(beta=0,J=1.3),
	C=list(beta=0,J=1.1),
	D=list(beta=0.25,J=0.1),
	E=list(beta=0.25,J=1.3),
	G=list(beta=0.25,J=1.3),
	F=list(beta=0,J=1)
)
load("evoarchdata/data/ceramics_lbk_merzbach.rda")
ceramics_lbk_merzbach=t(ceramics_lbk_merzbach[,2:ncol(ceramics_lbk_merzbach)])

allres=readRDS("abcRFmore.RDS")
allres=readRDS("abcRFsmaller.RDS")
allres=readRDS("abcRF100k_allmet.RDS")
allres=readRDS("abcRF5k_NMU.RDS")
allres=readRDS("RDS_files/abcRF150k_NMU.RDS")
alldismulti=allres[[1]]
betas=allres[[2]]
Js=allres[[3]]
Ns=allres[[4]]
mus=allres[[5]]
tsteps=allres[[6]]
disfunc=names(alldismulti[1,][[1]])
names(disfunc)=disfunc
LBks=sapply(disfunc,function(i)sapply(alldismulti[1,],"[[",i))



allsubsamp=readRDS("models_abcRF150k_NMU.RDS")[[2]]
allruns=readRDS("RDS_files/models_abcRF150k_NMU.RDS")[[1]]
projfol="NMU150k_wideTHS/"

models=names(allruns)
names(models)=models
names(allsubsamp)=names(allruns)
printAllstat(allsubsamp,projfol,"_subsample",originalparams)
printAllstat(allruns,projfol,originalparams=originalparams)
artifc=lapply(disfunc,function(i)sapply(alldismulti[2,],"[[",i))
artifc=lapply(artifc,function(rs){rownames(rs)=names(allruns);rs})
collec=lapply(models,function(m)sapply(artifc,function(i)unlist(i[m,])))
collec[["LBK"]]=LBks

```

play with rejection algorithm

```{r}

plot(density(Js[rank(collec[[1]][,"simpson"],ties.method="first")<2000]))

js=seq(0,2,.01)
bs=seq(0,2,.01)

for(m in names(collec)[1]){
    for( i in colnames(collec[[m]])){
        mean_img=matrix(0,ncol=length(js)-1,nrow=length(bs)-1)
        min_img=matrix(0,ncol=length(js)-1,nrow=length(bs)-1)
        for(j in 2:length(js)){
            for(b in 2:length(bs)){
                mean_img[j-1,b-1]=mean(collec$A[,i][ Js<js[j] & Js>=js[j-1] & betas<bs[b] & betas>=bs[b-1]],na.rm=T)
                min_img[j-1,b-1]=min(collec$A[,i][ Js<js[j] & Js>=js[j-1] & betas<bs[b] & betas>=bs[b-1]],na.rm=T)
            }
        }
        png(paste0(projfol,"means_model_",m,"_space_",i,".png"))
        image(x=js,y=bs,mean_img,zlim=allranges[,i],main=paste("mean distance to model",m,"using",i))
        dev.off()
        png(paste0(projfol,"min_model_",m,"_space_",i,".png"))
        image(x=js,y=bs,min_img,zlim=allranges[,i],main=paste("minimal distance to model",m,"using",i))
        dev.off()
    }
}

allrank=lapply(collec,function(m) apply(m,2,rank,ties.method="first"))
allmeanrank=lapply(collec,function(m) apply(m,1,mean))
averagebest=lapply(allmeanrank,function(m)rank(m)<500)

allbests=lapply(collec,function(m) apply(m,2,function(i)rank(i,ties.method="first")<10000))

cols=RColorBrewer::brewer.pal(length(models),"Set2")
names(cols)=models
cols[["LBK"]]="grey"

var=disfunc
gpe=list()
gpe[[1]]=c("A","B","C","D","E","F")
for( met in var){
    posteriorsJs=lapply(allbests,function(i)Js[i[,met]])
    posteriorsBeta=lapply(allbests,function(i)betas[i[,met]])
    posteriorsNs=lapply(allbests,function(i)Ns[i[,met]])
    posteriorsMus=lapply(allbests,function(i)mus[i[,met]])
    posteriorsTsteps=lapply(allbests,function(i)tsteps[i[,met]])
    for(m in models){
        png(paste0(projfol,"allposteriors_",met,"_",m,".png"),width=1000,height=1000)
        plot(cbind.data.frame(J=posteriorsJs[[m]],beta=posteriorsBeta[[m]],N=posteriorsNs[[m]],tstep=posteriorsTsteps[[m]],mu=posteriorsMus[[m]]),main=met)
        dev.off()
    }
}

for( met in var){
    print(paste0("problem",met))
    posteriorsJs=lapply(allbests,function(i)density(Js[i[,met]],from=0,to=2))
    posteriorsBeta=lapply(allbests,function(i)density(betas[i[,met]],from=0,to=2))
    posteriorsNs=lapply(allbests,function(i)density(Ns[i[,met]],from=500,to=1500))
    posteriorsMus=lapply(allbests,function(i)density(mus[i[,met]],from=0.06,to=0.12))
    posteriorsTsteps=lapply(allbests,function(i)density(tsteps[i[,met]],from=100,to=400))

#    png(paste0(projfol,met,"_posterios.png"),width=2400,height=800)
#    par(mfrow=c(1,5))
#    plot(1,1,ylim=range(sapply(posteriorsJs,"[[","y")),xlab="J",main="Results of ABC fitting for J",type="n",ylab="density",xlim=c(0,2))
#    lines(density((Js),from=0,to=2),col="grey",lwd=3)
#    lapply(names(posteriorsJs),function(n)lines(posteriorsJs[[n]],col=cols[[n]],lwd=3))
#    plot(1,1,ylim=c(0.1,max(sapply(posteriorsBeta,"[[","y"))),xlab=expression(beta),main="Results of ABC fitting for Beta",type="n",ylab="density",xlim=c(0,2),log="y")
#    lines(density((betas),from=0,to=2),col="grey",lwd=3)
#    lapply(names(posteriorsBeta),function(n)lines(posteriorsBeta[[n]],col=cols[[n]],lwd=3))
#
#    plot(1,1,ylim=range(sapply(posteriorsTsteps,"[[","y")),xlab="tstep",main="Results of ABC fitting for tsteps",type="n",ylab="density",xlim=c(100,400))
#    lines(density((tsteps),from=100,to=400),col="grey",lwd=3)
#    lapply(names(posteriorsTsteps),function(n)lines(posteriorsTsteps[[n]],col=cols[[n]],lwd=3))
#
#    plot(1,1,ylim=range(sapply(posteriorsMus,"[[","y")),xlab=expression(mu),main="Results of ABC fitting for mu",type="n",ylab="density",xlim=c(0.06,0.12))
#    lines(density((mus),from=0.06,to=0.12),col="grey",lwd=3)
#    lapply(names(posteriorsMus),function(n)lines(posteriorsMus[[n]],col=cols[[n]],lwd=3))
#
#    plot(1,1,ylim=range(sapply(posteriorsNs,"[[","y")),xlab="N",main="Results of ABC fitting for N",type="n",ylab="density",xlim=c(500,1500))
#    lines(density((Ns),from=500,to=1500),col="grey",lwd=3)
#    lapply(names(posteriorsNs),function(n)lines(posteriorsNs[[n]],col=cols[[n]],lwd=3))
#    legend("topright",legend=names(allbests)[-1],lwd=3,col=cols[-1])
#    dev.off()
#
#    png(paste0(projfol,met,"_2Dposterios.png"),width=1000,height=600,pointsize=15)
#
#    layout(mat=t(c(1,2)),widths=c(.65,.35))
#    par(mar=c(4,4,2,0))
#    plot(1,1,ylim=c(0,2),xlim=c(0,2),type="n",xlab="J",ylab=expression(beta))
#    lapply(gpe[[1]],function(n)points(Js[allbests[[n]][,met]],betas[allbests[[n]][,met]],col=adjustcolor(cols[[n]],.4),pch=20,cex=2))
#    lapply(gpe[[1]],function(i)points(originalparams[[i]]$J,originalparams[[i]]$beta,bg=cols[[i]],pch=21,cex=4))
#    legend("topleft",legend=gpe[[1]],pch=20,col=adjustcolor(cols[gpe[[1]]],.6),pt.cex=2,bg="white",ncol=2)
#    par(mar=c(4,0,2,0))
#    plot(rep(1,6),3:8,bg=cols[gpe[[1]]],pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
#    text(rep(1.5,6),3:8,as.expression(lapply(gpe[[1]],function(m)bquote(list(.(m)~" "~ J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
#    dev.off()

    png(paste0(projfol,met,"_2Dposterios_LBK.png"),width=1000,height=600,pointsize=15)

    layout(mat=t(c(1,2)),widths=c(.65,.35))
    par(mar=c(4,4,2,0))
    plot(1,1,ylim=c(0,2),xlim=c(0,2),type="n",xlab="J",ylab=expression(beta),main=met)
    lapply(gpe[[1]],function(n)points(Js[allbests[[n]][,met]],betas[allbests[[n]][,met]],col=adjustcolor(cols[[n]],.4),pch=20,cex=2))
    lapply("LBK",function(n)points(Js[allbests[[n]][,met]],betas[allbests[[n]][,met]],bg=adjustcolor(cols[[n]],.4),pch=21,cex=1))
    lapply(gpe[[1]],function(i)points(originalparams[[i]]$J,originalparams[[i]]$beta,bg=cols[[i]],pch=21,cex=4))
    legend("topleft",legend=gpe[[1]],pch=20,col=adjustcolor(cols[gpe[[1]]],.6),pt.cex=2,bg="white",ncol=2)
    par(mar=c(4,0,2,0))
    plot(rep(1,6),3:8,bg=cols[gpe[[1]]],pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
    points(1,2,bg=cols["LBK"],pch=21,cex=2,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
    text(rep(1.5,6),3:8,as.expression(lapply(gpe[[1]],function(m)bquote(list(.(m)~" "~ J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
    text(1.5,2,"LBK",adj=0)
    dev.off()

#    png(paste0(projfol,met,"_2Dposterios_Nmu.png"),width=1000,height=600,pointsize=15)
#    layout(mat=t(c(1,2)),widths=c(.65,.35))
#    par(mar=c(4,4,2,0))
#    plot(1,1,ylim=c(500,1500),xlim=c(0.06,0.12),type="n",xlab=expression(mu),ylab="N")
#    lapply(gpe[[1]],function(n)points(mus[allbests[[n]][,met]],Ns[allbests[[n]][,met]],col=adjustcolor(cols[[n]],.4),pch=20,cex=2))
#    lapply(gpe[[1]],function(i)points(0.1,1000,bg=cols[[i]],pch=21,cex=4))
#    legend("topleft",legend=gpe[[1]],pch=20,col=adjustcolor(cols[gpe[[1]]],.6),pt.cex=2,bg="white",ncol=2)
#    par(mar=c(4,0,2,0))
#    plot(rep(1,6),3:8,bg=cols[gpe[[1]]],pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
#    text(rep(1.5,6),3:8,as.expression(lapply(gpe[[1]],function(m)bquote(list(.(m)~" "~ J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
#    dev.off()
#
#    png(paste0(projfol,met,"_2Dposterios_tstepmu.png"),width=1000,height=600,pointsize=15)
#    layout(mat=t(c(1,2)),widths=c(.65,.35))
#    par(mar=c(4,4,2,0))
#    plot(1,1,ylim=c(100,400),xlim=c(0.06,0.12),type="n",xlab=expression(mu),ylab="tsteps")
#    lapply(gpe[[1]],function(n)points(mus[allbests[[n]][,met]],tsteps[allbests[[n]][,met]],col=adjustcolor(cols[[n]],.4),pch=20,cex=2))
#    lapply(gpe[[1]],function(i)points(0.1,300,bg=cols[[i]],pch=21,cex=4))
#    legend("topleft",legend=gpe[[1]],pch=20,col=adjustcolor(cols[gpe[[1]]],.6),pt.cex=2,bg="white",ncol=2)
#    par(mar=c(4,0,2,0))
#    plot(rep(1,6),3:8,bg=cols[gpe[[1]]],pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
#    text(rep(1.5,6),3:8,as.expression(lapply(gpe[[1]],function(m)bquote(list(.(m)~" "~ J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
#    dev.off()
#
#
}
for( met in var){
    png(paste0(projfol,met,"_2Dposterios_all.png"),width=1000,height=600,pointsize=15)

    layout(mat=t(c(1,2)),widths=c(.65,.35))
    par(mar=c(4,4,2,0))
    plot(1,1,ylim=c(0,2),xlim=c(0,2),type="n",xlab="J",ylab=expression(beta))
    lapply(names(allbests),function(n)points(Js[allbests[[n]][,met]],betas[allbests[[n]][,met]],col=adjustcolor(cols[[n]],.4),pch=20,cex=2))
    lapply(names(allbests),function(i)points(originalparams[[i]]$J,originalparams[[i]]$beta,bg=cols[[i]],pch=21,cex=4))
    legend("topleft",legend=names(allbests),pch=20,col=adjustcolor(cols[names(allbests)],.6),pt.cex=2,bg="white",ncol=2)
    par(mar=c(4,0,2,0))
    plot(rep(1,8),1:8,bg=rev(cols[names(allbests)]),pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
    text(rep(1.5,8),1:8,as.expression(lapply(rev(names(allbests)),function(m)bquote(list(.(m)~" "~ J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
    dev.off()
}


```


```{r}


library(abcrf)
obser=as.data.frame(t(sapply(disfunc,function(i)0)))
##as.data.frame(t(c(gap=0,unique=0,simpson=0,turnover=0,spectrum=0,gini=0)))
png(paste0(projfol,"model_full_LBK.png"),width=900,height=500)
par(mfrow=c(1,2))
dtSolo=cbind.data.frame(J=Js,b=betas,LBks)
testol=regAbcrf(b~.,dtSolo[1:50000,c("b",var)],ntree=100,paral=T,ncores=5)
valB=densityPlot(testol,as.data.frame(obser),dtSolo[,c("b",var)],main="LBK",xlab=expression(beta),from=0,to=2)
testolJ=regAbcrf(J~.,dtSolo[1:50000,c("J",var)],ntree=100,paral=T,ncores=5)
valJ=densityPlot(testol,as.data.frame(obser),dtSolo[,c("J",var)],main="LBK",xlab="J",from=0,to=2)
dev.off()

xnewJ=runif(1000,0,2)
xnewB=runif(1000,0,2)
yJ=ecdf(valJ$post$y)(xnewJ)
yB=ecdf(val$post$y)(xnewB)
plot(yJ,yB,ylim=c(0,2),xlim=c(0,2),pch=20,col=adjustcolor("red",.1))

lapply(names(models),function(m){
    dtSolo=cbind.data.frame(J=Js,b=betas,N=Ns,tstep=tsteps,mu=mus,collec[[m]][,var])
    png(paste0(projfol,"model_full_",m,".png"),width=900,height=500)
    par(mfrow=c(1,5))
    testol=regAbcrf(J~.,dtSolo[,c("J",var)])
    densityPlot(testol,obser,dtSolo[,c("J",var)],xlab="J",main=bquote(list(J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))
    testol=regAbcrf(b~.,dtSolo[,c("b",var)])
    densityPlot(testol,obser,dtSolo[,c("b",var)],xlab=expression(beta),main=bquote(list(J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))
    testol=regAbcrf(N~.,dtSolo[,c("N",var)])
    densityPlot(testol,obser,dtSolo[,c("N",var)],xlab="N",main=bquote(list(J== .(originalparams[[m]]$J)  , N == 1000)))
    testol=regAbcrf(mu~.,dtSolo[,c("mu",var)])
    densityPlot(testol,obser,dtSolo[,c("mu",var)],xlab=expression(mu),main=bquote(list(J== .(originalparams[[m]]$J)  , mu == 0.1)))
    testol=regAbcrf(tstep~.,dtSolo[,c("tstep",var)])
    densityPlot(testol,obser,dtSolo[,c("tstep",var)],xlab="tstep",main=bquote(list(J== .(originalparams[[m]]$J)  , tstep == 300)))
    dev.off()
} )


var=disfunc
allestimatepos=lapply(models,function(m){
#png(paste0(projfol,"model_full_",m,".png"),width=900,height=500)
#par(mfrow=c(1,2))
    dtSolo=cbind.data.frame(J=Js,b=betas,collec[[m]][,var])
    testol=regAbcrf(b~.,dtSolo[,c("b",var)],ntree=100,paral=T,ncores=5)
    valB=densityPlot(testol,as.data.frame(obser),dtSolo[,c("b","J",var)],xlab=expression(beta),from=0,to=2,main=bquote(list(.(m)~":"~J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))
    testol=regAbcrf(J~.,dtSolo[,c("J",var)],ntree=100,paral=T,ncores=5)
    valJ=densityPlot(testol,as.data.frame(obser),dtSolo[,c("J",var)],xlab="J",from=0,to=2,main=bquote(list(.(m)~":"~J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))
#dev.off()
return(list(b=valB,j=valJ))

})
names(allestimatepos)=names(models)
plot(1,1,type="n",ylim=c(0,2),xlim=c(0,2))
lapply(names(allestimatepos),function(m){
    j=allestimatepos[[m]]$j
    beta=allestimatepos[[m]]$b
    polygon(beta$post$y,j$post$y,col=adjustcolor(cols[[m]],.3))
})

plot(1,1,type="n",ylim=c(0,2),xlim=c(0,2),xlab="J",ylab=expression(beta),main="Sample of posterior for 5 artificial scenario")
empiricalestimates=lapply(names(models),function(m){
    yB=sample(x=allestimatepos[[m]]$b$post$x,size=500,prob=allestimatepos[[m]]$b$post$y,replace=T)
    yJ=sample(x=allestimatepos[[m]]$j$post$x,size=500,prob=allestimatepos[[m]]$j$post$y,replace=T)
    points(yJ,yB,bg=adjustcolor(cols[[m]],.3),pch=21,lwd=.2,col=1,cex=1.2)#cols[[m]])
})
lapply(names(models),function(m){
    points(allmodelsparams[[m]]$J+rnorm(1,0,.01),allmodelsparams[[m]]$beta+rnorm(1,0,.01),bg=cols[[m]],pch=21,lwd=.5,col=1,cex=4)#cols[[m]])
})


hdr2d=lapply(names(models),function(m){
    x=allestimatepos[[m]]$b$post$x
    zmat=matrix(ncol=length(x),nrow=length(x))
    yB=allestimatepos[[m]]$b$post$y
    yJ=allestimatepos[[m]]$j$post$y
    for(i in 1:length(x))
        for(j in 1:length(x))
            zmat[i,j]=yB[i]*yJ[j]
    zmat
})

x=valJ$post$x
lbkmat=matrix(ncol=length(x),nrow=length(x))
for(i in 1:length(x))
    for(j in 1:length(x))
        lbkmat[i,j]=valB$post$y[i]*valJ$post$y[j]

names(hdr2d)=names(models)
hdr2d[["LBK"]]=lbkmat

plot(1,1,type="n",ylim=c(0,2),xlim=c(0,2),xlab="J",ylab=expression(beta),main="posterior distributions for 5 artificial scenario")
lapply(names(models),function(m){
brks=seq(min(hdr2d[[m]]),max(hdr2d[[m]]),length.out=13)
image(x=x,y=x,t(hdr2d[[m]]),col=colorRampPalette(c(adjustcolor(cols[[m]],alpha.f=0),cols[[m]]),alpha=T)(12),add=T,breaks=brks)
contour(x=x,y=x,t(hdr2d[[m]]),col=1,add=T,lwd=.1,levels=brks,label="")
}
)


gpe=list(c("E","D","C","B","A"),c("A","E","C"))
cols=RColorBrewer::brewer.pal(length(models),"Set2")
names(cols)=models

png("posterior.png",width=900,height=600,pointsize=15)
layout(mat=t(c(1,2)),widths=c(.65,.35))
par(mar=c(4,4,2,0))
plot(1,1,type="n",ylim=c(0,2),xlim=c(0,2),xlab="J",ylab=expression(beta),main="Estimated posteriors for 5 artificial scenarios")
lapply(gpe[[1]],function(m){ brks=seq(min(hdr2d[[m]]),max(hdr2d[[m]]),length.out=13);image(x=x,y=x,t(hdr2d[[m]]),col=colorRampPalette(c(adjustcolor(cols[[m]],alpha.f=0),cols[[m]]),alpha=T)(12),add=T,breaks=brks);contour(x=x,y=x,t(hdr2d[[m]]),col=1,add=T,lwd=.1,levels=brks,label="",drawlabels = F) })
lapply(gpe[[1]],function(m) points(allmodelsparams[[m]]$J,allmodelsparams[[m]]$beta,bg=cols[[m]],pch=21,lwd=.5,col=1,cex=4))#cols[[m]]))
par(mar=c(4,0,2,0))
plot(rep(1,5),3:7,bg=cols[gpe[[1]]],pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
text(1.5,8,"original value",adj=-.2)
text(rep(1.5,5),3:7,as.expression(lapply(gpe[[1]],function(m)bquote(list(.(m)~" "~ J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
dev.off()

gpe=list(c("F","C","A"),c("A","E","C"))

png("posteriorBetaNull.png",width=900,height=600,pointsize=15)
layout(mat=t(c(1,2)),widths=c(.65,.35))
par(mar=c(4,4,2,0))
plot(1,1,type="n",ylim=c(0,2),xlim=c(0,2),xlab="J",ylab=expression(beta),main="Estimated posteriors for 5 artificial scenarios")
lapply(gpe[[1]],function(m){ brks=seq(min(hdr2d[[m]]),max(hdr2d[[m]]),length.out=13);image(x=x,y=x,t(hdr2d[[m]]),col=colorRampPalette(c(adjustcolor(cols[[m]],alpha.f=0),cols[[m]]),alpha=T)(12),add=T,breaks=brks);contour(x=x,y=x,t(hdr2d[[m]]),col=1,add=T,lwd=.1,levels=brks,label="",drawlabels = F) })
lapply(gpe[[1]],function(m) points(allmodelsparams[[m]]$J,allmodelsparams[[m]]$beta,bg=cols[[m]],pch=21,lwd=.5,col=1,cex=4))#cols[[m]]))
par(mar=c(4,0,2,0))
plot(rep(1,length(gpe[[1]])),4:6,bg=cols[gpe[[1]]],pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
text(1.5,7,"original value",adj=-.2)
text(rep(1.5,length(gpe[[1]])),4:6,as.expression(lapply(gpe[[1]],function(m)bquote(list(.(m)~" "~ J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
dev.off()




gpe=list(c("E","D","C","B","A","LBK"),c("A","E","C"))
cols=RColorBrewer::brewer.pal(length(models),"Set2")
names(cols)=models
cols[["LBK"]]="red"

png("posteriorWithLBK.png",width=900,height=600,pointsize=15)
layout(mat=t(c(1,2)),widths=c(.65,.35))
par(mar=c(4,4,2,0))
plot(1,1,type="n",ylim=c(0,2),xlim=c(0,2),xlab="J",ylab=expression(beta),main="Estimated posteriors for 5 artificial scenarios")
lapply(gpe[[1]],function(m){ brks=seq(min(hdr2d[[m]]),max(hdr2d[[m]]),length.out=13);image(x=x,y=x,t(hdr2d[[m]]),col=colorRampPalette(c(adjustcolor(cols[[m]],alpha.f=0),cols[[m]]),alpha=T)(12),add=T,breaks=brks);contour(x=x,y=x,t(hdr2d[[m]]),col=1,add=T,lwd=.1,levels=brks,label="",drawlabels = F) })
lapply(gpe[[1]],function(m) points(allmodelsparams[[m]]$J,allmodelsparams[[m]]$beta,bg=cols[[m]],pch=21,lwd=.5,col=1,cex=4))#cols[[m]]))
par(mar=c(4,0,2,0))
plot(rep(1,6),2:7,bg=cols[gpe[[1]]],pch=21,cex=6,ylim=c(0,9),xlim=c(0,4),ann=F,axes=F)
text(1.5,8,"original value",adj=-.2)
text(rep(1.5,6),2:7,as.expression(lapply(gpe[[1]],function(m)bquote(list(.(m)~" "~ J== .(originalparams[[m]]$J)  , beta == .(originalparams[[m]]$beta))))),adj=0)
dev.off()

originalparams[["LBK"]]$J="?"
originalparams[["LBK"]]$beta="?"





var=c("gap","simpson","unique")
allprediction=lapply(names(models),function(m){
    dtSolo=cbind.data.frame(J=Js,b=betas,collec[[m]][,var])
    cbind(J=regAbcrf(J~.,dtSolo[,c("J",var)])$model.rf$predictions,beta=regAbcrf(b~.,dtSolo[,c("b",var)])$model.rf$predictions)
} )


#################################
test=readRDS(file="10000runs_allmodels.RDS")
allres=cbind(t(test[[1]]),beta=test[[2]],J=test[[3]])
allres=apply(allres,2,unlist)
test2=readRDS(file="10000runs_allmodels2.RDS")
allres2=cbind(t(test2[[1]]),beta=test2[[2]],J=test2[[3]])
allres2=apply(allres2,2,unlist)
test3=readRDS(file="150000runs_allmodels.RDS")
allres3=cbind(t(test3[[1]]),beta=test3[[2]],J=test3[[3]])
allres3=apply(allres3,2,unlist)
allres=rbind(allres,allres2,allres3)
allbest=lapply(c("LBK","A","B","C","D"),function(n)rank(allres[,n],ties.method='first')<2000)
names(allbest)=c("LBK","A","B","C","D")
Js=allres[,"J"]
beta=allres[,"beta"]
```

And plot the posterior distributions:

```{r posteriorJ,fig.cap="ABC results",cache=F}
cols=rainbow(length(allbest))
names(cols)=names(allbest)
par(mfrow=c(1,2))
posteriorsJs=lapply(allbest,function(i)density(Js[i],from=0,to=2))
posteriorsBeta=lapply(allbest,function(i)density(beta[i],from=0,to=2))

plot(1,1,ylim=range(sapply(posteriorsJs,"[[","y")),xlab="J",main="Results of ABC fitting for J",type="n",ylab="density",xlim=c(0,2))
lines(density((Js),from=0,to=2),col="grey",lwd=3)
lapply(names(posteriorsJs)[-1],function(n)lines(posteriorsJs[[n]],col=cols[[n]],lwd=3))

plot(1,1,ylim=c(0.1,max(sapply(posteriorsBeta,"[[","y"))),xlab=expression(beta),main="Results of ABC fitting for Beta",type="n",ylab="density",xlim=c(0,2),log="y")
lines(density((beta),from=0,to=2),col="grey",lwd=3)
lapply(names(posteriorsBeta)[-1],function(n)lines(posteriorsBeta[[n]],col=cols[[n]],lwd=3))
legend("topright",legend=names(allbest)[-1],lwd=3,col=cols[-1])

par(mfrow=c(1,2))
posteriorsJs=lapply(allbest,function(i)density(Js[i],from=0,to=2))
posteriorsBeta=lapply(allbest,function(i)density(beta[i],from=0,to=2))

plot(1,1,ylim=range(sapply(posteriorsJs,"[[","y")),xlab="J",main="Results of ABC fitting for J",type="n",ylab="density",xlim=c(0,2))
lines(density((Js),from=0,to=2),col="grey",lwd=3)
lapply(names(posteriorsJs),function(n)lines(posteriorsJs[[n]],col=cols[[n]],lwd=3))

plot(1,1,ylim=c(0.1,max(sapply(posteriorsBeta,"[[","y"))),xlab=expression(beta),main="Results of ABC fitting for Beta",type="n",ylab="density",xlim=c(0,2),log="y")
lines(density((beta),from=0,to=2),col="grey",lwd=3)
lapply(names(posteriorsBeta),function(n)lines(posteriorsBeta[[n]],col=cols[[n]],lwd=3))
legend("topright",legend=names(allbest),lwd=3,col=cols)

plot(1,1,ylim=c(0,2),xlim=c(0,2),type="n",xlab="J",ylab=expression(beta))
lapply(names(allbest),function(n)points(Js[allbest[[n]]],beta[allbest[[n]]],col=adjustcolor(cols[[n]],.2),pch=20,cex=1.5))
lapply(names(originalparams),function(i)points(originalparams[[i]]$J,originalparams[[i]]$beta,bg=cols[[i]],pch=21,cex=4))
legend("topleft",legend=names(allbest),pch=20,col=adjustcolor(cols,.6),pt.cex=2,bg="white",ncol=2)
plot(1:4,bg=cols[2:5],pch=21,cex=10,ylim=c(-1,5),xlim=c(-1,5))
)

quantile(Js[r1])
quantile(Js[r2])
```


Finding summary metrics

```{r}
relfreq=t(apply(ceramics_lbk_merzbach,1,function(i)i/apply(ceramics_lbk_merzbach,2,sum)))
rankfreq=apply(relfreq,2,rank,ties.method="max")
cols=rainbow(nrow(relfreq))
plot(1,1,type="n",ylim=range(c(rankfreq,5)),xlim=c(0,(ncol(rankfreq)+1)),xlab="phase",ylab="rank",main="Rank through time",yaxt="n")
lapply(1:nrow(rankfreq),function(r)points(rankfreq[r,],col=cols[r],lwd=2.5,type="b",pch=20,cex=.4))
axis(2,at=seq(5,36,length.out=5),label=round(1+(36-seq(5,36,length.out=5))))
legend("bottomleft",legend=rownames(ceramics_lbk_merzbach),col=cols,pch=20,ncol=8)

relrange=apply(relfreq,2,range)
relrange=apply(relrange,2,function(i)i[2]-i[1])
plot(relrange,ylim=c(0,1),xlim=c(0,(length(relrange)+1)),xlab="phase",ylab="% max-min",main="Gap between rank 1 and last rank",type="o",lwd=3)

uniquepos=apply(relfreq,2,function(i)sum(i>0))
plot(uniquepos,ylim=c(0,nrow(ceramics_lbk_merzbach)),xlim=c(0,(length(uniquepos)+1)),xlab="phase",ylab="# of unique traits",main="Number of unique traits in use through time",type="o",lwd=3)


play with frequency
```{r}
###
allspec=sapply(lapply(allsimpson,GeneCycle::periodogram),"[[","spec")
allfreq=sapply(lapply(allsimpson,GeneCycle::periodogram),"[[","freq")
plot(1,1,ylim=range(allspec),xlim=range(allfreq))
apply(allspec,2,function(i)lines(allfreq[,1],i))

```

```
